"""
Base classes
"""
from dataclasses import dataclass


@dataclass
class Problem:
    name: str
